## Chapter 4: The Material Balance for Chemical Reactors

The material here is the R version of Octave/Matlab code from the [website](http://jbrwww.che.wisc.edu/home/jbraw/chemreacfun/figures.html) for the book: Chemical Reactor Analysis and Design Fundamentals by James B. Rawlings and John G. Ekerdt.

### Figure 4.15-4.18: Semi-batch Reactor Polymerization with Volume Change

The reaction is a monomer $M$ going to a polymer $P$.
$$ M \rightarrow P $$

There is feed of monomer into the reactor at rate $Q_f$

The equation for monomer moles $M$ in reactor is:
$$ \frac{dM}{dt}=c_fQ_f-kM,\;\; M(0)=0 $$
The equation for volume in reactor $V$ is:
$$ \frac{dV}{dt}=Q_f+\Delta VkM, \;\;V(0)=V_0 $$

Two operations are simulated:
* Operation 1:
 * Feed rate $Q_f=1\; m^3/min$ until reactor volume $V=20\;m^3$ and then feed rate is set to zero.

* Operation 2:
 * Feed rate $Q_f=1\; m^3/min$ until reactor volume $V=20\;m^3$ and then feed rate is set such that the reactor volume stays constant at $20\;m^3$


This is solved analytically in the text and in the website. But here it is solved numerically. It uses the R package [deSolve](http://cran.r-project.org/web/packages/deSolve/index.html) and illustrates deSolve capabilities to solve ODE with events and roots.


```r
library(ggplot2)
library(deSolve)

k=0.1 # rate const 1/min
rhos=0.9e3 # density of solvent kg/m3
rhom=0.8e3 # density of monomer kg/m3
rhop=1.1e3 # density of polymer kg/m3

VR=20 # reactor volume m3
Qf0=1 # feed rate of monomer m3/min
Mm=100 # molecular wt of monomer kg/kmol
M0=0 # initial monomer amount in reactor kmol
V0=10 # Initial reactor volume m3
deltaV=(1/rhop-1/rhom)*Mm # m3/kmol
cMf=rhom/Mm # feed monomer concentration kmol/m3

# Governing rate equations
ratefn=function(t,y,parms){
  
  # state variables
  M=y[1]
  P=y[2]
  V=y[3]
  sw=y[4] # switch variable
  
  # parameters
  Qf0=parms["Qf0"]
  deltaV=parms["deltaV"]
  k=parms["k"]
  cMf=parms["cMf"]
  Mm=parms["Mm"]
  
  # feed is turned off when switch variable is one (rector volume is 20m3)
  if(sw == 1) { Qf0=0 }
  
  # rate of change calculation
  dM=Qf0*cMf-k*M
  dP=k*Mm*M
  dV=Qf0+deltaV*k*M
  dsw=0
  
  return(list(c(M=dM,P=dP,V=dV,sw=dsw)))
  
}

# Governing rate equations when feed rate is adjusted to keep reactor volume constant
ratefn2=function(t,y,parms){
  
  # state variable
  M=y[1]
  P=y[2]
  V=y[3]
  sw=y[4] # switch variable: does not play any role
  
  # parameters
  Qf0=parms["Qf0"]
  deltaV=parms["deltaV"]
  k=parms["k"]
  cMf=parms["cMf"]
  Mm=parms["Mm"]
  
  # feed rate to ensure rate of volume change is zero
  Qf0=-deltaV*k*M 
  
  # rate of change calculation
  dM=Qf0*cMf-k*M
  dP=k*Mm*M
  dV=0
  dsw=0
  
  return(list(c(M=dM,P=dP,V=dV,sw=dsw)))
  
}

# Simulation of Operation 1
# feed at Qf0 until reactor volume = 20m3 and shut the feed
yinit=c(M=M0,P=0,V=V0,sw=0)
parms=c(Qf0=Qf0,deltaV=deltaV,k=k,cMf=cMf,Mm=Mm)

# root function (getting the time when reactor reaches volume of 20m3)
root=function(t,y,parms) {y[3]-20}

# event function (switch variable is turned on when reactor reaches 20m3)
event=function(t,y,parms){
  
  y=y
  y[4]=1
  
  return(y)
  
}

# time span
times=seq(0,50,0.1)
yout=lsoda(y=yinit,times=times,func=ratefn,parms=parms,
           events=list(func=event,root=TRUE),rootfun=root)
# calculate feed profile
Qf=rep(Qf0,nrow(yout))
Qf[yout[,c("sw")]==1]=0
yout=cbind(yout,Qf=Qf)
# check output
head(yout)
```

```
##      time     M      P    V sw Qf
## [1,]  0.0 0.000 0.0000 10.0  0  1
## [2,]  0.1 0.796 0.3987 10.1  0  1
## [3,]  0.2 1.584 1.5894 10.2  0  1
## [4,]  0.3 2.364 3.5643 10.3  0  1
## [5,]  0.4 3.137 6.3155 10.4  0  1
## [6,]  0.5 3.902 9.8354 10.5  0  1
```

```r
tail(yout)
```

```
##        time     M    P    V sw Qf
## [496,] 49.5 1.174 8859 18.2  1  0
## [497,] 49.6 1.162 8861 18.2  1  0
## [498,] 49.7 1.151 8862 18.2  1  0
## [499,] 49.8 1.139 8863 18.2  1  0
## [500,] 49.9 1.128 8864 18.2  1  0
## [501,] 50.0 1.117 8865 18.2  1  0
```

```r

# Simulation of Operation 2
# Feed at Qf0 until reactor volume is 20m3 and after that feed at a rate to keep reactor
# volume constant
#
yinit=c(M=M0,P=0,V=V0,sw=0)
parms=c(Qf0=Qf0,deltaV=deltaV,k=k,cMf=cMf,Mm=Mm)
# simulation until reactor volume is 20m3 with feed rate of Qf0
times=seq(0,50,0.1)
yout2=lsoda(y=yinit,times=times,func=ratefn,parms=parms,
           rootfun=root)
# Simulation after reactor volume reaches 20m3 and feed rate is adjusted
# to keep reactor volume constant

# initial point is the final point from previous simulation
yinit=yout2[nrow(yout2),c("M","P","V","sw")]
tinit=yout2[nrow(yout2),1]
times=seq(0,50-round(tinit,1),0.1)
yout2b=lsoda(y=yinit,times=times,func=ratefn2,parms=parms)
yout2b[,1]=yout2b[,1]+tinit
yout2full=rbind(yout2,yout2b)
# calculate feed profile
Qf=apply(yout2full,1,function(x) {
                      Qf=Qf0
                      if(x["V"]>=20){
                        Qf=-deltaV*k*x["M"]
                      }
                      return(Qf)
                    })
yout2full=cbind(yout2full,Qf=Qf)

# plot volume vs time for both operations
plot(yout[,c("time")],yout[,c("V")],type="l",xlab="Time (min)",ylab="Reactor Volume (m3)")
lines(yout2full[,c("time")],yout2full[,c("V")],lty=2)
text(40,18,"Operation 1")
text(40,20,"Operation 2")
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-11.png) 

```r

# plot feed rate vs time for both operations
plot(yout[,c("time")],yout[,c("Qf")],type="l",xlab="Time (min)",ylab="Feed rate (m3/min)")
lines(yout2full[,c("time")],yout2full[,c("Qf")],lty=2)
text(10,0.05,"Operation 1")
text(30,0.1,"Operation 2")
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-12.png) 

```r


# plot monomer (kg) vs time for both operations
plot(yout[,c("time")],yout[,c("M")]*Mm,type="l",xlab="Time (min)",ylab="Monomer (kg)")
lines(yout2full[,c("time")],yout2full[,c("M")]*Mm,lty=2)
text(20,1000,"Opertion 1")
text(40,1000,"Operation 2")
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-13.png) 

```r


# plot polymer (kg) vs time for both operations
ymax=max(yout2full[,c("P")])*1.1
plot(yout[,c("time")],yout[,c("P")],type="l",ylim=c(0,ymax),
       xlab="Time (min)",ylab="Polymer (kg)")
lines(yout2full[,c("time")],yout2full[,c("P")],lty=2)
text(40,8000,"Operation 1")
text(20,9000,"Operation 2")
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-14.png) 

```r

```



## Session Info
This is created with Rstudio version 0.98.191 and knitr



```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] deSolve_1.10-6  ggplot2_0.9.3.1 knitr_1.2      
## 
## loaded via a namespace (and not attached):
##  [1] colorspace_1.2-2   dichromat_2.0-0    digest_0.6.3      
##  [4] evaluate_0.4.3     formatR_0.8        grid_3.0.1        
##  [7] gtable_0.1.2       labeling_0.2       MASS_7.3-26       
## [10] munsell_0.4        plyr_1.8           proto_0.3-10      
## [13] RColorBrewer_1.0-5 reshape2_1.2.2     scales_0.2.3      
## [16] stringr_0.6.2      tools_3.0.1
```



