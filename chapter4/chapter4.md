## Chapter 4: The Material Balance for Chemical Reactors

The material here is the R version of Octave/Matlab code from the [website](http://jbrwww.che.wisc.edu/home/jbraw/chemreacfun/figures.html) for the book: Chemical Reactor Analysis and Design Fundamentals by James B. Rawlings and John G. Ekerdt.

### Figure 4.3,4.4: First-order, irreversible kinetics in a batch reactor

For a first order irreversible reaction $A\rightarrow B$ with rate constant $k$, the concentration profile of $A$ over time is:
$$
c_A=c_{A0}e^{-kt}
$$

The profile is plotted for different values of k


```r
# time
t=seq(0,5,0.1)
# rate constants
k=c(0.5,1,2,5)
# get concentration relative to initial concentration for each rate constant
cA=sapply(k,function(k) exp(-k*t))

# put in a dataframe
df=data.frame(cbind(t,cA))
names(df)=c("t","0.5","1","2","5")

# load plotting library
library(ggplot2)
# data reshaping library
library(reshape2)

# reshape for plotting
df2=melt(df,id.vars=c("t"),variable.name="k",value.name="conc")

#plot
ggplot(data=df2,aes(x=t,y=conc,color=k))+geom_line(size=1)+
  xlab("time")+ylab("cA/cA0")+theme_bw()
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-11.png) 

```r

#plot in log scale for concentration
ggplot(data=df2,aes(x=t,y=conc,color=k))+geom_line(size=1)+
  xlab("time")+ylab("cA/cA0")+scale_y_log10()+theme_bw()
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-12.png) 

```r

```


## Figure 4.5: First order, reversible kinetics in a batch reactor

The reaction in this case is:
$$  A \overset{k_1}{\underset{k_{-1}}{\Longleftrightarrow}} B $$

The profile of $c_A$ and $c_B$ is given by:
$$
c_A=c_{A0}e^{-(k_1+k_{-1})t}+\frac{k_{-1}}{k_1+k_{-1}}(c_{A0}+c_{B0})[1-e^{-(k_1+k_{-1})t}] \\
c_B=c_{A0}+c_{B0}-c_A
$$


```r
# define rate constants
k1=1  # forward rate constant
km1=0.5 # reverse rate constant

# define initial conc
cA0=1
cB0=0

# conc profile
t=seq(0,5,0.1)
cA=cA0*exp(-(k1+km1)*t)+(km1/(k1+km1))*(cA0+cB0)*(1-exp(-(k1+km1)*t))
cB=cA0+cB0-cA

dfA=data.frame(time=t,conc=cA,cmp=rep("A",length(t)))
dfB=data.frame(time=t,conc=cB,cmp=rep("B",length(t)))
df=rbind(dfA,dfB)

ggplot(df,aes(x=time,y=conc,color=cmp))+geom_line(size=1)+xlab("time")+ylab("conc")+
   theme_bw(20)
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2.png) 


## Figure 4.6: Second order and First order kinetics in a batch reactor

The reaction in this case is:
$$ A \rightarrow B $$
For a first order reaction, the concentration profile is:
$$ c_A=c_{A0}e^{-kt} $$
For a second order reaction, the concentration profile is:
$$ c_A=\frac{c_{A0}}{c_{A0}+kt} $$


```r
# rate const for first order reaction 
k1=1
# rate const for second order reaction
k2=1
# initial condition such that initial rate is same as for first order rxn
cA0=1

# profile of cA over cA0 over time
# first order rxn
t=seq(0,5,0.1)
cAn1=exp(-k1*t)
# second order rxn
cAn2=1/(cA0+k2*t)

# plot results
plot(t,cAn1,type="l",col="red",xlab="time",ylab="cA/cA0")
lines(t,cAn2,col="blue")
text(1,0.2,"first order")
text(3,0.4,"second,order")
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png) 


## Figure 4.7,4.10: Reaction rate versus concentration for nth-order kinetics

Rate is given by $r=kc_A^n$. 


```r
# rate vs conc for k=1

npts=100
cfin=3
cmin=0.001
k=1

# reaction orders
orders=c(3,2,1,0.5,0,-0.5,-1,-2)

# range of cA
cA=seq(0.001,3,0.1)

# calculation of rate
r=sapply(orders,function(x) k*cA^x)

#plotting rate vs cA
df=data.frame(cbind(cA,r))
names(df)=c("cA",orders)

df2=melt(df,id.vars=c("cA"),variable.name="order",value.name="rate")
df2$order=as.numeric(as.character(df2$order))

# plot of reaction rate vs conc for n>=0
ggplot(data=df2[df2$order>=0,],aes(x=cA,y=rate,color=factor(order)))+geom_line(size=1)+ylim(c(0,5))+theme_bw(20)
```

```
## Warning: Removed 19 rows containing missing values (geom_path).
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-41.png) 

```r

# plot of reaction rate vs conc for n<=0
ggplot(data=df2[df2$order<=0,],aes(x=cA,y=rate,color=factor(order)))+geom_line(size=1)+ylim(c(0,5))+theme_bw(20)
```

```
## Warning: Removed 8 rows containing missing values (geom_path).
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-42.png) 

```r

```


## Figure 4.8, 4.9: Batch Reactor with nth order kinetics

For a reaction $A \rightarrow Product$ following nth order kinetics, the concentration profile is:
$$ \frac{c_A}{c_{A0}}=[1+(n-1)k_0t]^{\frac{1}{-n+1}},\;\;n\neq 1 $$ where $k_0=kc_{A0}^{n-1}$


```r
# set k and ca0
k=1
ca0=1

# list of orders
n=c(-2,-1,-0.5,0,0.5,1,2,3,4,5)

# set time span
t=c(seq(0,2,length.out=100),seq(2.1,5,length.out=100))

conc=sapply(n,function(x){
                  k0=k*ca0^(x-1) 
                  if (x == 1){
                    c=exp(-k*t)
                  } else {
                    c=(1+(x-1)*k0*t)^(1/(-x+1))
                      }
                  return(c)
                })

df=data.frame(cbind(t,conc))
names(df)=c("time",n)
df2=melt(df,c("time"),variable.name="order",value.name="conc")

# plot for orders 1,2,3,4,5
ggplot(data=df2[df2$order %in% c(1,2,3,4,5),],aes(x=time,y=conc,color=order))+
   geom_line(size=1)+xlab("Time")+ylab("cA/cA0")+theme_bw(20)
```

![plot of chunk unnamed-chunk-5](figure/unnamed-chunk-51.png) 

```r

# plot for orders -2,-1,-0.5,0,0.5,1,2
ggplot(data=df2[df2$order %in% c(-2,-1,-0.5,0,0.5,1,2),],aes(x=time,y=conc,color=order))+
   geom_line(size=1)+xlab("Time")+xlim(c(0,2))+ylim(c(0,1))+ylab("cA/cA0")+theme_bw(20)
```

```
## Warning: Removed 974 rows containing missing values (geom_path).
```

![plot of chunk unnamed-chunk-5](figure/unnamed-chunk-52.png) 

```r

```


## Figure 4.11: Two First Order Reactions in Series in a Batch Reactor

Reactions in series are:
$$
A \rightarrow B \\
B \rightarrow C
$$

The expression for $c_A$ and $c_B$ is:
$$
c_A=c_{A0}e^{-k_1t} \\
c_B=c_{B0}e^{-k_2t}+c_{A0}\frac{k_1}{k_2-k_1}[e^{-k_1t}-e^{-k_2t}], \;\; k_1\neq k_2 \\
c_C=c_{A0}+c_{B0}+c_{C0}-c_A-c_B
$$


```r
# initial conc
cA0=1
cB0=0
cC0=0

# rate constants
k1=2
k2=1

# time span
t=seq(0,5,0.1)

# cA
cA=cA0*exp(-k1*t)
# cB
cB=cB0*exp(-k2*t)+cA0*(k1/(k2-k1))*(exp(-k1*t)-exp(-k2*t))
# cC
cC=cA0+cB0+cC0-cA-cB

df=data.frame(cbind(t,cA,cB,cC))
names(df)=c("t","cA","cB","cC")
df2=melt(df,c("t"),variable.name="species",value.name="conc")
 
ggplot(data=df2,aes(x=t,y=conc,color=species))+geom_line(size=1)+
  xlab("time")+ylab("concentration")+theme_bw(20)
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6.png) 


## Figure 4.12: Two First-order Reactions in Parallel in a Batch Reactor

The reactions are:
$$
A \overset{k_1}{\rightarrow} B \\
A \overset{k_2}{\rightarrow} C
$$

The expressions for concentrations vs time are:

$$
c_A=c_{A0}e^{-(k_1+k_2)t} \\
c_B=c_{B0}+c_{A0}\frac{k_1}{k_1+k_2}(1-e^{-(k_1+k_2)t}) \\
c_C=c_{A0}+c_{B0}+c_{C0}-c_A-c_B
$$


```r
# set initial conc
cA0=1
cB0=0
cC0=0

# set rate constants
k1=1
k2=2

# time span
t=seq(0,3,0.1)
# conc profile vs time
cA=cA0*exp(-(k1+k2)*t)
cB=cB0+cA0*(k1/(k1+k2))*(1-exp(-(k1+k2)*t))
cC=cA0+cB0+cC0-cA-cB

df=data.frame(cbind(t,cA,cB,cC))
names(df)=c("t","cA","cB","cC")
df2=melt(df,c("t"),variable.name="species",value.name="conc")

#plot conc profile
ggplot(data=df2,aes(x=t,y=conc,color=species))+geom_line(size=1)+
  xlab("time")+ylab("concentration")+theme_bw(20)
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7.png) 


## Figure: 4.14: Reaching Steady State in a CSTR


```r
# set initial concentration
ca0=0
# residence time (min)
tau=100
# rate constant
k=0.1
# feed concentration of A (gmol/lit)
caf=2 

# steady state concentration
cSS=caf/(1+k*tau)

# dynamics of reaching steady state

# load ode solving library
library(deSolve)

# function definining the ODE
rateeq=function(t,ca,parms){
  caf=parms["caf"]
  k=parms["k"]
  dcdt=(1/tau)*(caf-ca)-k*ca
  return(list(dcdt))
}

# solve the ODE with condition of ca0=0
tspan=seq(0,120,0.1)
parms=c(caf=caf,k=k)
ca0=0 # initial condition
c1=lsoda(ca0,tspan,rateeq,parms)

# solve the ODE with condition of ca0=2
tspan=seq(0,120,0.1)
parms=c(caf=caf,k=k)
ca0=2 # initial condition
c2=lsoda(ca0,tspan,rateeq,parms)

df=data.frame(c1,c2[,2])
names(df)=c("time","ca0=0","ca0=2")
df2=melt(df,c("time"),variable.name="Initial_cA",value.name="cA")
ggplot(df2,aes(x=time,y=cA,color=Initial_cA))+geom_line(size=1)+
  geom_hline(aes(yintercept=cSS),linetype=2,size=1)+theme_bw(20)
```

![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-8.png) 

```r

```


## Session Info
This is created with Rstudio version 0.98.191 and knitr


```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] deSolve_1.10-6  reshape2_1.2.2  ggplot2_0.9.3.1 knitr_1.2      
## 
## loaded via a namespace (and not attached):
##  [1] colorspace_1.2-2   dichromat_2.0-0    digest_0.6.3      
##  [4] evaluate_0.4.3     formatR_0.8        grid_3.0.1        
##  [7] gtable_0.1.2       labeling_0.2       MASS_7.3-26       
## [10] munsell_0.4        plyr_1.8           proto_0.3-10      
## [13] RColorBrewer_1.0-5 scales_0.2.3       stringr_0.6.2     
## [16] tools_3.0.1
```



