## Chapter 2: The Stoichiometry of Reactions

The material here is the R version of Octave/Matlab code from the [website](http://jbrwww.che.wisc.edu/home/jbraw/chemreacfun/figures.html) for the book: Chemical Reactor Analysis and Design Fundamentals by James B. Rawlings and John G. Ekerdt.

### Figure 2.2/2.3: Estimated reaction rates from production rates


```r

# set stoichiometric matrix
stoi=matrix(c(0,1,0,-1,-1,1,
              -1,1,1,-1,0,0,
              1,0,-1,0,-1,1),nrow=3,byrow=TRUE)

# set reaction rates
r=c(1,2)

A=stoi[1:2,]

# production rate of components
R=t(A)%*%r

# generate a data set of measured production rates by
# adding random noise to actual production rate determined above

sdR=0.05 # std deviation of measurement noise
set.seed(100) # set seed to enable reproducing the data

npoints=500
Rmeas=matrix(0,nrow=npoints,ncol=nrow(R))
for (i in 1:npoints){
  Rmeas[i,]=R+rnorm(nrow(R),mean=0,sd=sdR)
}

# Estimate reaction rate from production rate data
rest=t(solve(A%*%t(A))%*%(A%*%t(Rmeas)))

# plot estimated reaction rates
plot(rest[,1],rest[,2],xlab=expression(r[1]),ylab=expression(r[2]),
     xlim=c(0.8,1.2),ylim=c(1.8,2.2))
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1.png) 


## Session Info
This is created with Rstudio version 0.98.191 and knitr


```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] knitr_1.2
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    stringr_0.6.2 
## [5] tools_3.0.1
```

