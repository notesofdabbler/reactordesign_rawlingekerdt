## Chapter 3: Review of Chemical Equilibrium

The material here is the R version of Octave/Matlab code from the [website](http://jbrwww.che.wisc.edu/home/jbraw/chemreacfun/figures.html) for the book: Chemical Reactor Analysis and Design Fundamentals by James B. Rawlings and John G. Ekerdt.

### Figure 3.3: Gibbs Energy vs Reaction Extent $\epsilon$

Modifed Gibbs energy for a gas phase reaction system is:
$$
\tilde{G}=-\epsilon'lnK+(1+\overline{\nu}\epsilon')lnP+
    \sum_j(y_{j0}+\nu_j\epsilon')
    ln\left(\frac{y_{j0}+\nu_j\epsilon'}{1+\overline{\nu}\epsilon'}\right)
$$    

For the reaction $I+B \Leftrightarrow P$, $\overline{\nu}=-1$. For a starting mixture with $y_{I0}=0.5,y_{B0}=0.5,y_{P0}=0$, the modified Gibbs energy is computed and plotted.


```r
# set K and P
K=108
P=2.5

# reaction stoichiometry
nu=rep(0,3)
nu[1]=-1
nu[2]=-1
nu[3]=1
nubar=-1

# initial mole fraction
y0=rep(0,3)
y0[1]=0.5
y0[2]=0.5
y0[3]=0

npts=100
x=seq(0,0.5,length.out=npts)

G=-x*log(K)+(1+nubar*x)*log(P)
for (j in 1:3){
  
  # calculate G for 0 < x < 0.5
  xint=x[2:(npts-1)]
  G[2:(npts-1)]=G[2:(npts-1)]+(y0[j]+nu[j]*xint)*log((y0[j]+nu[j]*xint)/(1+nubar*xint))
  
  # calculate G for x=0
  if(y0[j]+nu[j]*x[1] > 1e-10){
    G[1]=G[1]+(y0[j]+nu[j]*x[1])*log((y0[j]+nu[j]*x[1])/(1+nubar*x[1]))
  }
  
  # calculate G for x=0.5
  if(y0[j]+nu[j]*x[npts] > 1e-10){
    G[npts]=G[npts]+(y0[j]+nu[j]*x[npts])*log((y0[j]+nu[j]*x[npts])/(1+nubar*x[npts]))
  }
  
}

# plot G vs extent   
plot(x,G,type="l",xlab=expression(epsilon),ylab=expression(tilde(G)))
par(fig=c(0.4,1,0.4,1),new=TRUE)
plot(x,G,type="l",xlim=c(0.45,0.5),ylim=c(-1.95,-1.88),
     xlab=expression(epsilon),ylab=expression(tilde(G)))
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1.png) 

```r
par(fig=c(0,1,0,1))
```


## Figure 3.4: Partial pressures of components A and C versus liquid-phase composition in a nonideal solution

For a non-ideal solution in equilibrium with ideal vapor for a binary system of A and C, the following relations hold:
$$
p_A=p_A^{sat}\gamma_Ax_A \\
p_B=p_B^{sat}\gamma_Bx_B
$$
For an ideal solution system $\gamma_A=1, \; \gamma_B=1$. The activity coefficient based on Margules equation is:
$$
ln\gamma_A=[A_{AC}+2(A_{CA}-A_{AC})x_A]x_C^2 \\
ln\gamma_C=[A_{CA}+2(A_{AC}-A_{CA})x_C]x_A^2 \\
$$


```r
# range of x_A values
x_A=seq(0,1,length.out=100)
x_C=1-x_A

# parameters of margules equation
A_AC=1.4
A_CA=2

# vapor pressure
pA_sat=0.65
pC_sat=0.5

# activity coefficient 
lngamma_A=(A_AC+2*(A_CA-A_AC)*x_A)*x_C^2
lngamma_C=(A_CA+2*(A_AC-A_CA)*x_C)*x_A^2

# ideal partial pressure
pA_id=pA_sat*x_A
pC_id=pC_sat*x_C
ptot_id=pA_id+pC_id

# partial pressure of nonideal solution
pA=pA_sat*x_A*exp(lngamma_A)
pC=pC_sat*x_C*exp(lngamma_C)
ptot=pA+pC

# plot partial pressure vs xA
plot(x_A,pA_id,type="l",lty=2,ylim=c(0,max(pA,pC,ptot)*1.1),
     xlab=expression(x[A]),ylab="P (atm)")
lines(x_A,pC_id,lty=2)
lines(x_A,ptot_id,lty=2)
lines(x_A,pA,col="blue")
lines(x_A,pC,col="blue")
lines(x_A,ptot,col="blue")
text(0.1,0.2,expression(P[A]),col="blue")
text(0.9,0.2,expression(P[B]),col="blue")
text(0.4,0.9,expression(P[tot]),col="blue")
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2.png) 


## Figure 3.6: Gibbs Energy Contour as a Function of Two Reaction Extents

The equilibrium is obtained by minimizing:
$$
\tilde{G}=-\sum_i\epsilon'_ilnK_i+(1+\sum_i\overline{\nu_i}\epsilon'_i)lnP+
           \sum_j(y_{j0}+\sum_i\nu_{ij}\epsilon'_i)
                 ln\left[\frac{y_{0j}+\sum_i\nu_{ij}\epsilon'_i}
                        {1+\sum_i\overline{\nu_i}\epsilon'_i}\right]
$$
In this example there are two reactions:
$$
I+B \Leftrightarrow P1\\
I+B \Leftrightarrow P2 
$$ with initial conditions $y_I=y_B=0.5,y_{P1}=0,y_{P2}=0$. The constraints on reaction extent in this case are:
$$
\epsilon'_1+\epsilon'_2 \leq 0.5,\;\;,\epsilon'_1 \geq 0,\;\; \epsilon'_2 \geq 0
$$


```r

library(alabama)
```

```
## Loading required package: numDeriv
```

```r

# reaction and system parameters
deltag1=-3.72 # kcal/mol
deltag2=-4.49 # kcal/mol
T=400 #K
P=2.5 #K
R=1.987 #cal/mol.K
K1=exp(-deltag1*1000/(R*T))
K2=exp(-deltag2*1000/(R*T))
K=c(K1,K2)

# Initial molefraction
y0=rep(0,4)
y0[1]=0.5
y0[2]=0.5

# stoichiometric matrix
nu=matrix(c(-1,-1,1,0,
            -1,-1,0,1),nrow=2,byrow=TRUE)
# mole change for each reaction
nubar=rowSums(nu)

# function to calculate G
Gval=function(w){
  
  nr=length(w)
  nc=length(y0)
  
  G=-sum(w*log(K))+(1+sum(nubar*w))*log(P)
  
  for(j in 1:nc){
    if(y0[j]+sum(nu[,j]*w) >= 1e-5){
      G=G+(y0[j]+sum(nu[,j]*w))*log((y0[j]+sum(nu[,j]*w))/(1+sum(nubar*w)))
        }
    }
  
  return(G)
  
}

# inequalities that should be satisfied
hineq=function(w){
  h=rep(0,3)
  h[1]=-w[1]-w[2]+0.5
  h[2]=w[1]
  h[3]=w[2]
  return(h)
}

# initial guess of reaction extent
winit=c(0.01,0.01)
# find reaction extent that minimizes G subject to constraints
wopt=constrOptim.nl(par=winit,fn=Gval,hin=hineq)
```

```
## Min(hin):  0.01 
## par:  0.01 0.01 
## fval:  0.01775 
## Min(hin):  0.01604 
## par:  0.1331 0.3508 
## fval:  -2.559 
## Min(hin):  0.016 
## par:  0.1332 0.3509 
## fval:  -2.559
```

```r
# optimal solution
wopt$par
```

```
## [1] 0.1332 0.3509
```

```r

# Contour plot of G with overlay of minimum
ext1=seq(0.01,0.49,by=0.01)
ext2=seq(0.01,0.49,by=0.01)
cntrdf=expand.grid(ext1,ext2)
names(cntrdf)=c("ext1","ext2")
cntrdf$G=apply(cntrdf,1,function(x) Gval(x))
cntrdf$G[cntrdf$ext1+cntrdf$ext2>0.5]=NA
z=matrix(cntrdf$G,nrow=length(ext1))
filled.contour(ext1,ext2,z,xlab=expression(epsilon[1]),ylab=expression(epsilon[2]),
               plot.axes={axis(1);axis(2);
                         contour(ext1,ext2,z,levels=c(-1,-1.5,-2,-2.5,-2.53,-2.55),
                                 labcex=1,lwd=2,add=TRUE);
                         points(wopt$par[1],wopt$par[2],pch=19,col="blue")})
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png) 

```r

```


## Session Info
This is created with Rstudio version 0.98.191 and knitr


```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] alabama_2011.9-1  numDeriv_2012.9-1 knitr_1.2        
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    stringr_0.6.2 
## [5] tools_3.0.1
```

